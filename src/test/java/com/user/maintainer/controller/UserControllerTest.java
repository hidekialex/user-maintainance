package com.user.maintainer.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.user.maintainer.controller.request.UserRequest;
import com.user.maintainer.service.UserService;

public class UserControllerTest {

	@InjectMocks
	private UserController controller;
	
	@Mock
	private UserService service;
	
	private MockMvc mockMvc;
	
	private ObjectMapper objectMapper;
	
	private static final String PATH_CREATE_USER = "/user";
	private static final String PATH_GET_USER = "/user/{userId}";
	private static final String PATH_UPDATE_USER = "/user/{userId}";
	private static final String PATH_DELETE_USER = "/user/{userId}";
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		this.objectMapper = new ObjectMapper();
	}
	
	@Test
	public void createUser() throws Exception {
		
		UserRequest request = new UserRequest();
		request.setAddress("address");
		request.setBirthDate(LocalDate.now());
		request.setLastName("lastName");
		request.setName("name");
		request.setPhoneNumbers(Arrays.asList("1199996666"));
		
		
		mockMvc.perform(post(PATH_CREATE_USER).content(objectMapper.writeValueAsString(request))
				.header("Content-Type", "application/json")).andExpect(status().isCreated());
		
		verify(service, times(1)).createUser(any(UserRequest.class));
	}
	
	@Test
	public void getUser() throws Exception {
		
		String userId = "123456";
		
		mockMvc.perform(get(PATH_GET_USER, userId).header("Content-Type", "application/json")).andExpect(status().isOk());
		
		verify(service, times(1)).getUser(eq(userId));
		
	}
	
	@Test
	public void updateUser() throws Exception {
		
		String userId = "123456";
		
		UserRequest request = new UserRequest();
		request.setAddress("address");
		request.setBirthDate(LocalDate.now());
		request.setLastName("lastName");
		request.setName("name");
		request.setPhoneNumbers(Arrays.asList("1199996666"));
		
		mockMvc.perform(put(PATH_UPDATE_USER, userId).content(objectMapper.writeValueAsString(request))
				.header("Content-Type", "application/json")).andExpect(status().isOk());
		
		verify(service, times(1)).updateUser(eq(userId), any(UserRequest.class));
	}
	
	@Test
	public void deleteUser() throws Exception {
		
		String userId = "123456";
		
		mockMvc.perform(delete(PATH_DELETE_USER, userId).header("Content-Type", "application/json")).andExpect(status().isOk());
		
		verify(service, times(1)).deleteUser(eq(userId));
	}
}
