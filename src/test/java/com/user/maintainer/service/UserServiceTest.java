package com.user.maintainer.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.user.maintainer.controller.exception.ResourceNotFoundException;
import com.user.maintainer.controller.request.UserRequest;
import com.user.maintainer.controller.response.CreateUserResponse;
import com.user.maintainer.controller.response.DeleteUserResponse;
import com.user.maintainer.controller.response.UpdateUserResponse;
import com.user.maintainer.controller.response.UserResponse;
import com.user.maintainer.entity.User;
import com.user.maintainer.repository.UserRepository;


public class UserServiceTest {

	@InjectMocks
	private UserService service;
	
	@Mock
	private UserRepository repository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void createUser() {

		UserRequest request = new UserRequest();
		
		User user = new User();
		user.setId("12345");
		when(repository.save(any(User.class))).thenReturn(user);
		
		CreateUserResponse response = service.createUser(request);
		
		verify(repository, times(1)).save(any(User.class));
		
		assertEquals("12345", response.getId());
	}
	
	@Test
	public void getUser() throws ResourceNotFoundException {
		
		String userId = "userId";
		
		User user = new User();
		user.setId("12345");
		user.setAddress("address");
		user.setLastName("lastName");
		user.setName("name");
		user.setPhoneNumbers(Arrays.asList("1199996666"));
		when(repository.findOne(eq(userId))).thenReturn(user);
		
		UserResponse response = service.getUser(userId);
		assertEquals("address", response.getAddress());
		assertEquals("lastName", response.getLastName());
		assertEquals("name", response.getName());
		assertEquals("1199996666", response.getPhoneNumbers().get(0));
		
		verify(repository, times(1)).findOne(eq(userId));
	}
	
	@Test
	public void updateUser() throws ResourceNotFoundException {
		
		String userId = "userId";
		UserRequest request = new UserRequest();
		
		User user = new User();
		user.setId("12345");
		user.setAddress("address");
		user.setLastName("lastName");
		user.setName("name");
		user.setPhoneNumbers(Arrays.asList("1199996666"));
		when(repository.findOne(eq(userId))).thenReturn(user);
		
		UpdateUserResponse response = service.updateUser(userId, request);
		
		assertEquals("Usuario atualizado com sucesso.", response.getMessage());
		
		verify(repository, times(1)).findOne(eq(userId));
		verify(repository, times(1)).save(any(User.class));
	}
	
	@Test
	public void deleteUser() throws ResourceNotFoundException {
		
		String userId = "userId";
		
		User user = new User();
		user.setId("12345");
		user.setAddress("address");
		user.setLastName("lastName");
		user.setName("name");
		user.setPhoneNumbers(Arrays.asList("1199996666"));
		when(repository.findOne(eq(userId))).thenReturn(user);
		
		DeleteUserResponse response = service.deleteUser(userId);
		
		assertEquals("Usuario deletado com sucesso.", response.getMessage());
		
		verify(repository, times(1)).findOne(eq(userId));
		verify(repository, times(1)).delete(any(User.class));
	}
}