package com.user.maintainer.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.maintainer.controller.exception.ResourceNotFoundException;
import com.user.maintainer.controller.request.UserRequest;
import com.user.maintainer.controller.response.CreateUserResponse;
import com.user.maintainer.controller.response.DeleteUserResponse;
import com.user.maintainer.controller.response.UpdateUserResponse;
import com.user.maintainer.controller.response.UserResponse;
import com.user.maintainer.entity.User;
import com.user.maintainer.repository.UserRepository;

@Service
public class UserService {

	private final UserRepository repository;
	
	@Autowired
	public UserService(final UserRepository repository) {
		this.repository = repository;
	}
	
	public CreateUserResponse createUser(UserRequest request) {
		User user = new User(request);
		User userSaved = repository.save(user);
		return new CreateUserResponse(userSaved.getId(), "User", "http://localhost:8080/user/".concat(userSaved.getId()));
	}

	public UserResponse getUser(String userId) throws ResourceNotFoundException {
		User user = verifyAndGetUser(userId);
		return new UserResponse(user);
	}

	public UpdateUserResponse updateUser(String userId, UserRequest request) throws ResourceNotFoundException {
		User user = verifyAndGetUser(userId);
		User userToUpdate = new User(request);
		userToUpdate.setId(user.getId());
		repository.save(userToUpdate);
		return new UpdateUserResponse("Usuario atualizado com sucesso.");
	}

	public DeleteUserResponse deleteUser(String userId) throws ResourceNotFoundException {
		User user = verifyAndGetUser(userId);
		repository.delete(user);
		return new DeleteUserResponse("Usuario deletado com sucesso.");
	}
	
	private User verifyAndGetUser(String userId) throws ResourceNotFoundException {
		Optional<User> userOpt = Optional.ofNullable(repository.findOne(userId));
		if(!userOpt.isPresent()) {
			throw new ResourceNotFoundException("Usuario não existe.");
		}
		return userOpt.get();
	}
}