package com.user.maintainer.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFormatter {

	public enum DateFormat {
		
		YYYY_MM_DD("yyyy-MM-dd");

		private String format;

		DateFormat(String format) {
			this.format = format;
		}

		public String getFormat() {
			return format;
		}
	}
	
	public static String format(LocalDate localDate, DateFormat format) {
		return localDate.format(DateTimeFormatter.ofPattern(format.getFormat())).toString();
	}
	
	public static LocalDate toLocalDate(String date, DateFormat pattern) {
		try {
			DateTimeFormatter format = DateTimeFormatter.ofPattern(pattern.getFormat());
			return LocalDate.parse(date, format);
		} catch (Exception e) {
			return null;
		}
	}
}