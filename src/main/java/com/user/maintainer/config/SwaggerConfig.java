package com.user.maintainer.config;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

	@Bean
	public Docket newsApi() {
		return new Docket(SWAGGER_2)
				.ignoredParameterTypes(ApiIgnore.class)
				.apiInfo(apiInfo())
				.select()
				.paths(validPaths())
				.build();
	}

	@SuppressWarnings("unchecked")
	public Predicate<String> validPaths() {
		return or(regex("/user.*"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("User Maintainance Service Application")
				.description("It's an RESTful service provides a user maintainance service.")
				.version("1.0")
				.build();
	}
}
