package com.user.maintainer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.user.maintainer.entity.User;

public interface UserRepository extends MongoRepository<User, String>{

}
