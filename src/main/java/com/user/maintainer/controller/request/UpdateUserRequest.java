package com.user.maintainer.controller.request;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.annotations.ApiModelProperty;

public class UpdateUserRequest {

	@ApiModelProperty(dataType = "String", required = true, value = "Nome do usuário")
	private String name;
	
	@ApiModelProperty(dataType = "String", required = true, value = "Sobrenome do usuário")
	private String lastName;
	
	@JsonDeserialize(using = CustomLocalDateDeserializer.class)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(dataType = "Date", required = true, value = "Data de Nascimento do usuário", allowableValues = "yyyy-MM-dd", example = "yyyy-MM-dd")
	private LocalDate birthDate;
	
	@ApiModelProperty(dataType = "String", required = true, value = "Endereço do usuário")
	private String address;
	
	@ApiModelProperty(dataType = "String", required = true, value = "Números de telefone do usuário")
	private List<String> phoneNumbers;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}