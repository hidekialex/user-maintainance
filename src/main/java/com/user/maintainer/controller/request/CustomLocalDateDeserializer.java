package com.user.maintainer.controller.request;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.user.maintainer.utils.DateFormatter;

public class CustomLocalDateDeserializer  extends JsonDeserializer<LocalDate> {

	@Override
	public LocalDate deserialize(JsonParser parser, DeserializationContext context)	throws IOException, JsonProcessingException {
		 String date = parser.getText();
	        try {
	            return DateFormatter.toLocalDate(date, DateFormatter.DateFormat.YYYY_MM_DD);
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
		}
}