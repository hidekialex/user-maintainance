package com.user.maintainer.controller.response;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.user.maintainer.utils.DateFormatter;

public class CustomLocalDateSerializer extends JsonSerializer<LocalDate> {

	@Override
	public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider arg2) throws IOException, JsonProcessingException {
		String formattedDate = DateFormatter.format(value, DateFormatter.DateFormat.YYYY_MM_DD);
		gen.writeString(formattedDate);
	}
}
