package com.user.maintainer.controller.response;

public class CreateUserResponse {

	private String id;

	private String rel;

	private String href;
	
	public CreateUserResponse(String id, String rel, String href) {
		this.id = id;
		this.rel = rel;
		this.href = href;
	}
 
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
}