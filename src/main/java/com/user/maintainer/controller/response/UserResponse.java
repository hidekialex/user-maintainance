package com.user.maintainer.controller.response;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.user.maintainer.entity.User;

public class UserResponse {

	private String name;
	
	private String lastName;
	
	@JsonSerialize(using = CustomLocalDateSerializer.class)
	private LocalDate birthDate;
	
	private String address;
	
	private List<String> phoneNumbers;
	
	public UserResponse(User user) {
		setName(user.getName());
		setLastName(user.getLastName());
		setBirthDate(user.getBirthDate());
		setAddress(user.getAddress());
		setPhoneNumbers(user.getPhoneNumbers());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}