package com.user.maintainer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.user.maintainer.controller.exception.ResourceNotFoundException;
import com.user.maintainer.controller.request.UserRequest;
import com.user.maintainer.controller.response.CreateUserResponse;
import com.user.maintainer.controller.response.DeleteUserResponse;
import com.user.maintainer.controller.response.UpdateUserResponse;
import com.user.maintainer.controller.response.UserResponse;
import com.user.maintainer.service.UserService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/user")
public class UserController {

	private final UserService service;
	
	@Autowired
	public UserController(final UserService service) {
		this.service = service;
	}
	
	@ApiOperation(value = "Cria um usuário", response = CreateUserResponse.class, produces = "application/json", consumes = "application/json", httpMethod = "POST")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public CreateUserResponse createUser(@Valid @RequestBody UserRequest request) {
		return service.createUser(request);
	}
	
	@ApiOperation(value = "Retorna um usuário a partir de um id", response = UserResponse.class, produces = "application/json", consumes = "application/json", httpMethod = "GET")
	@RequestMapping(path = "/{userId}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public UserResponse getUser(@PathVariable String userId) throws ResourceNotFoundException {
		return service.getUser(userId);
	}
	
	@ApiOperation(value = "Atualiza dados de um usuário existente", response = UpdateUserResponse.class, produces = "application/json", consumes = "application/json", httpMethod = "PUT")
	@RequestMapping(path = "/{userId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public UpdateUserResponse updateUser(@PathVariable String userId, @Valid @RequestBody UserRequest request) throws ResourceNotFoundException {
		return service.updateUser(userId, request);
	}
	
	@ApiOperation(value = "Remove um usuário a partir de um id", response = DeleteUserResponse.class, produces = "application/json", consumes = "application/json", httpMethod = "DELETE")
	@RequestMapping(path = "/{userId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public DeleteUserResponse deleteUser(@PathVariable String userId) throws ResourceNotFoundException {
		return service.deleteUser(userId);
	}
}