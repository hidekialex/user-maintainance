package com.user.maintainer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserMaintainerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserMaintainerApplication.class, args);
	}
}
