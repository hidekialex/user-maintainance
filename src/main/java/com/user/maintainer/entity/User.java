package com.user.maintainer.entity;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.user.maintainer.controller.request.UserRequest;

@Document
public class User {

	private String id;
	
	private String name;
	
	private String lastName;
	
	private LocalDate birthDate;
	
	private String address;
	
	private List<String> phoneNumbers;

	public User() {}
	
	public User(UserRequest request) {
		setName(request.getName());
		setLastName(request.getLastName());
		setBirthDate(request.getBirthDate());
		setAddress(request.getAddress());
		setPhoneNumbers(request.getPhoneNumbers());
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}
	
	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}