# Para executar a aplicação: 

Antes de executar o comando abaixo garanta que tenha o **Docker** instalado e rodando na máquina (Para garantir o correto funcionamento seria interessante baixar todos os containers ativos, caso houver). Vá até a raiz do projeto (onde está o arquivo pom.xml) e execute o seguinte comando:

```shell
mvn docker:stop docker:start spring-boot:run
```

A documentação estará em: http://localhost:8080/swagger-ui.html#!/

## Para executar em produção:

Basta trocar a string de conexão do mongo no profile "prod" do arquivo **application.yml** que se encontra dentro da pasta **resources** do projeto e executar o comando na raiz do projeto:

```shell
mvn package
```

Despois vá até o diretório target e execute: 

```shell
java -jar user-maintainer-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod
```